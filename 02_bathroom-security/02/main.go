/*
This program reads in a string of directions
as runes: U, D, L, R
and moves a position on a keypad.

Then pressing each key that is stopped upon.

When a direction would otherwise leave the keypad
it is skipped.

The program returns the keycode derived from the instructions.

2016 rob j loranger
*/

package main

import (
	"bufio"
	"fmt"
	"os"
)

type point struct {
	x, y int
}

var crazypad = [5][5]int{
	{0, 0, 1, 0, 0},      // rows padded with 0
	{0, 2, 3, 4, 0},      // to denote 'edge'
	{5, 6, 7, 8, 9},      // X for letters
	{88, 65, 66, 67, 88}, // ascii X, A, B, C, X
	{88, 88, 68, 88, 88}, // acsii X, X, D, X, X
}

// current key
// starts at key '5'
// x is along row, y is row
var key = point{0, 2}

func main() {
	// code is keypad sequence
	var code []int
	// get input from stdin
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		for _, v := range scanner.Text() {
			move(v, &key)
		}
		// enter key into sequence
		code = append(code, crazypad[key.y][key.x])
	}
	if err := scanner.Err(); err != nil {
		fmt.Printf("scanner error reading stdin: %v", err)
	}
	fmt.Println(code)
}

func move(d rune, k *point) {
	if d == 'U' && k.y > 0 {
		if crazypad[k.y-1][k.x] != 0 && crazypad[k.y-1][k.x] != 88 {
			k.y--
		}
	} else if d == 'D' && k.y < 4 {
		if crazypad[k.y+1][k.x] != 0 && crazypad[k.y+1][k.x] != 88 {
			k.y++
		}
	} else if d == 'R' && k.x < 4 {
		if crazypad[k.y][k.x+1] != 0 && crazypad[k.y][k.x+1] != 88 {
			k.x++
		}
	} else if d == 'L' && k.x > 0 {
		if crazypad[k.y][k.x-1] != 0 && crazypad[k.y][k.x-1] != 88 {
			k.x--
		}
	}
}
