# Advent of Code 2016

My participation in the [Advent of Code](http://adventofcode.com) 2016. Solutions written in go.

All puzzle directories are separated into their two parts, 01 and 02:
```
01_puzzle\
  01\ //part one
  02\ //part two
```

## Notes on usage:

I'm using STDIN to pipe input to the binaries, so:
`go run main.go < ../input` is typically how I run them.
