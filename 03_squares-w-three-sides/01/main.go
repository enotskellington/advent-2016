// Now that you can think clearly, you move deeper into the labyrinth of hallways and office furniture that makes up this part of Easter Bunny HQ. This must be a graphic design department; the walls are covered in specifications for triangles.
// Or are they?
// The design document gives the side lengths of each triangle it describes, but... 5 10 25? Some of these aren't triangles. You can't help but mark the impossible ones.
// In a valid triangle, the sum of any two sides must be larger than the remaining side. For example, the "triangle" given above is impossible, because 5 + 10 is not larger than 25.
// In your puzzle input, how many of the listed triangles are possible?

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var count int

type Triangle struct {
	a, b, c int
}

func (t Triangle) IsTriangle() bool {
	if t.a+t.b > t.c && t.b+t.c > t.a && t.c+t.a > t.b {
		return true
	}
	return false
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var set Triangle
	for scanner.Scan() {
		line := scanner.Text()
		set.a, _ = strconv.Atoi(strings.Trim(line[:5], " "))
		set.b, _ = strconv.Atoi(strings.Trim(line[5:10], " "))
		set.c, _ = strconv.Atoi(strings.Trim(line[10:], " "))
		if set.IsTriangle() {
			count++
		}
	}
	fmt.Printf("Total true triangles: %v\n", count)
}
