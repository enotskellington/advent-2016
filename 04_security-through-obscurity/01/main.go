package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var sum int

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	// For every line in input
	for scanner.Scan() {
		// get a string
		line := scanner.Text()
		length := len(line) // store the length
		// slice out checksum, sector id and room name
		checksum, name := line[length-6:length-1], line[:length-11]
		sectorid, err := strconv.Atoi(line[length-10 : length-7])
		if err != nil {
			fmt.Printf("couldn't convert string to int: %v\n", err)
		}
		// empty array for a-z counts
		letters := [26]int{}
		// count all characters, skipping '-' hyphens
		for _, c := range name {
			if c != '-' {
				// subtract 97 to make ascii lowercase letters 0-25 to match array idx
				letters[c-97]++
			}
		}
		// somewhere to store the calculated checksum
		var actualsum string
		// generate actual check sum to compare
		// go over letters, keeping higher. at end zero highest after setting in checksum
		for i := 0; i < 5; i++ {
			highest := 0
			for j := 1; j < 26; j++ {
				if letters[j] > letters[highest] {
					highest = j
				}
			}
			actualsum += string(highest + 97)
			letters[highest] = 0
		}
		if checksum == actualsum {
			sum += sectorid
		}
	}
	fmt.Printf("Sum of true room sector id's: %v\n", sum)
}
